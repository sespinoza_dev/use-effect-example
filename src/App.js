import React, { useState, useEffect } from 'react'

const App = () => {
  const [query, setQuery] = useState('')
  const [peopleList, setPeopleList] = useState(null)

  useEffect(() => {
    const fethPeople = async () => {
      const response = await fetch(`https://swapi.dev/api/people/?search=${query}`)
      const resJson =  await response.json()
      setPeopleList(resJson)
    }

    fethPeople()
  }, [query])

  const handleChange = (event) => setQuery(event.target.value)

  return (
    <>
      <input onChange={handleChange}></input>
      <div>
        {
          peopleList && peopleList.count > 0 &&
            peopleList.results.map((user, key) => (
              <div key={key}>
                <p>{user.name}</p>
              </div>
          ))
        }
      </div>
    </>
  )
}

export default App
