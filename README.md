# README

This is a simple fetch app example using both useState and useEffect hooks.

You can find the API used [here](https://swapi.dev/documentation).

## Installations

```
yarn
```

## Available Scripts

In the project directory, you can run:

### `npm start`

```
yarn start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Contributors

- [[sespinoza_dev]](https://gitlab.com/sespinoza_dev) S.Espinoza - creator, maintainer

